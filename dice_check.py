from embit import bip39

#! change this with the results of the 99 dice rolls
roll_data = ("555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555")

if len(roll_data) != 99:
    print("Error. Roll data must contain 99 numbers. It has %d" % len(roll_data))
else:
    roll_data = roll_data.replace("6", "0")
    entropy_integer = int(roll_data,6) 
    entropy_bytes = entropy_integer.to_bytes(32, byteorder="little")
    print(bip39.mnemonic_from_bytes(entropy_bytes).split())